# leanfuse

FUSE driver for LEAN 0.7. Build with `make`. `cc` and `libfuse` are mandatory dependencies.

## Usage

To create an image, use `leanfuse -Cimgnamehere`:

	$ ./leanfuse -Ctest.img
	Enter block size (must be PoT and >=256) 512
	Enter size of volume in blocks: 50000
	Enter superblock ID (must be <=32) 32
	Enter backup superblock ID (0 to set to the last block of the first band, which is recommended): 0
	Enter first bitmap portion block (0 means to put directly after superblock, which is recommended): 0
	Enter ID of root inode (0 means to put directly after the first bitmap portion block, which is recommended): 0
	$

Here is an example with mounting:

	$ #The optional -O argument sets the offset of the volume in the image.
	$ ./leanfuse -O0 mnt test.img
	$ ls mnt
	boo
	$ cat mnt/boo
	asdf
	asdf
	$ echo "Test" > mnt/test
	$ ls mnt
	boo  test
	$ umount mnt