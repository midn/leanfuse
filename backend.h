#ifndef _BACKEND_H
#define _BACKEND_H

#include<stdint.h>
#include<stddef.h>
#include<sys/stat.h>
#include<fuse.h>

#define EXTENTS_PER_INODE 6

#define INODE_ATTRIBUTE_iaInlineExtAttr (1 << 19)
#define INODE_ATTRIBUTE_iaFmtRegular (1 << 29)
#define INODE_ATTRIBUTE_iaFmtDirectory (2 << 29)
#define INODE_ATTRIBUTE_iaFmtMask (7 << 29)

struct Superblock {
	uint32_t checksum;
	uint32_t magic;
	uint16_t fsVersion;
	uint8_t preallocCount;
	uint8_t logBlocksPerBand;
	uint32_t state;
	uint8_t uuid[16];
	char volumeLabel[64];
	uint64_t blockCount;
	uint64_t freeBlockCount;
	uint64_t primarySuper;
	uint64_t backupSuper;
	uint64_t bitmapStart;
	uint64_t rootInode;
	uint64_t badInode;
	uint64_t journalInode;
	uint8_t logBlockSize;
	uint8_t reserved0[7];
} __attribute__((packed, aligned(4)));

struct INode {
	size_t id;
	struct {
		uint32_t checksum;
		uint32_t magic;
		uint8_t extentCount;
		uint8_t reserved[3];
		uint32_t indirectCount;
		uint32_t linkCount;
		uint32_t uid;
		uint32_t gid;
		uint32_t attributes;
		uint64_t fileSize;
		uint64_t blockCount;
		int64_t accessTime;
		int64_t statusChangeTime;
		int64_t modificationTime;
		int64_t creationTime;
		uint64_t firstIndirect;
		uint64_t lastIndirect;
		uint64_t fork;
		uint64_t extentStarts[EXTENTS_PER_INODE];
		uint32_t extentSizes[EXTENTS_PER_INODE];
	} __attribute__((packed)) raw;
};

struct DirectoryEntry {
	uint64_t inode;
	uint8_t type;
	uint8_t recLen;
	uint16_t nameLen;
	char name[];
} __attribute__((packed));

struct Backend {
	int fd;
	uint64_t offset;
	struct Superblock superblock;
};

extern struct Backend backend;

int lean_get_inode(size_t id, struct INode *out);
void lean_save_inode(const struct INode *in);
void *lean_read_inode(struct INode *inode, size_t off, size_t len);
void lean_write_inode(struct INode *inode, size_t off, const char *data, size_t len);
int lean_get_inode_from_path(const char *path, struct INode *out);
int lean_load_superblock();
int lean_create_inode(struct INode *node);
int lean_is_block_used(size_t block);
int lean_extend_node(struct INode *node, size_t blocks);
int lean_shrink_node(struct INode *node, size_t blocks);
size_t lean_find_free_blocks(int minSize, int maxSize, int *foundSize);
void lean_link(struct INode *parent, struct INode *child, const char *name);
void lean_unlink(struct INode *parent, struct INode *child, const char *name);
uint32_t lean_compute_checksum(const uint32_t *buf, size_t dwordLength, size_t dwordPadTo);
void lean_mark_block(size_t block, int used);
void lean_cleanup();

#endif