#include"backend.h"

#include<libgen.h>
#include<stdio.h>
#include<fuse.h>
#include<sys/stat.h>
#include<errno.h>
#include<string.h>
#include<stdlib.h>
#include<signal.h>
#include<inttypes.h>
#include<unistd.h>

int fe_getattr(const char *path, struct stat *out) {
	//~ printf("getattr %s\n", path);
	
	memset(out, 0, sizeof(*out));
	
	struct INode node;
	if(lean_get_inode_from_path(path, &node) == 0) return -ENOENT;
	
	out->st_nlink = node.raw.linkCount;
	out->st_uid = node.raw.uid;
	out->st_gid = node.raw.gid;
	out->st_size = node.raw.fileSize;
	
	/* TODO: FUSE includes leap seconds, while LEAN doesn't. FIX. */
	out->st_atim.tv_sec = node.raw.accessTime / 1000000;
	out->st_atim.tv_nsec = (node.raw.accessTime % 1000000) * 1000;
	
	out->st_mtim.tv_sec = node.raw.modificationTime / 1000000;
	out->st_mtim.tv_nsec = (node.raw.modificationTime % 1000000) * 1000;
	
	out->st_ctim.tv_sec = node.raw.statusChangeTime / 1000000;
	out->st_ctim.tv_nsec = (node.raw.statusChangeTime % 1000000) * 1000;
	
	/* Fast copy POSIX permissions. */
	out->st_mode |= node.raw.attributes & ((1 << 12) - 1);
	
	if((node.raw.attributes & INODE_ATTRIBUTE_iaFmtMask) == INODE_ATTRIBUTE_iaFmtRegular) {
		out->st_mode |= S_IFREG;
	} else if((node.raw.attributes & INODE_ATTRIBUTE_iaFmtMask) == INODE_ATTRIBUTE_iaFmtDirectory) {
		out->st_mode |= S_IFDIR;
	}
	
	return 0;
}

int fe_open(const char *path, struct fuse_file_info *fi) {
	//~ printf("open %s\n", path);
	
	struct INode node;
	if(lean_get_inode_from_path(path, &node) == 0) return -ENOENT;
	
	fi->fh = node.id;
	
	return 0;
}

int fe_read(const char *path, char *dst, size_t len, off_t off, struct fuse_file_info *fi) {
	//~ printf("read %s %lu %lu\n", path, off, len);
	
	struct INode node;
	if(lean_get_inode(fi->fh, &node) == 0) return -ENOENT;
	
	void *src = lean_read_inode(&node, off, len);
	memcpy(dst, src, len);
	free(src);
	
	return len;
}

int fe_readdir(const char *path, void *out, fuse_fill_dir_t filldir, off_t, struct fuse_file_info *fi) {
	//~ printf("readdir %s\n", path);
	
	struct INode node;
	if(lean_get_inode_from_path(path, &node) == 0) return -ENOENT;
	
	char *entries = lean_read_inode(&node, 0, node.raw.fileSize);
	for(size_t i = 0; i < node.raw.fileSize;) {
		struct DirectoryEntry *e = (void*) &entries[i];
		
		if(e->type == 0) goto skip;
		
		char *name = malloc(e->nameLen + 1);
		
		memcpy(name, e->name, e->nameLen);
		name[e->nameLen] = 0;
		if(filldir(out, name, NULL, 0)) break;
		free(name);
		
skip:
		i += e->recLen << 4;
	}
	
	return 0;
}

int fe_mknod(const char *path, mode_t mode, dev_t dev) {
	//~ printf("mknod %s\n", path);
	
	int typeFlag = 0;
	if(mode & S_IFREG) {
		typeFlag = INODE_ATTRIBUTE_iaFmtRegular;
	} else if(mode & S_IFDIR) {
		typeFlag = INODE_ATTRIBUTE_iaFmtDirectory;
	} else {
		return -EINVAL;
	}
	
	struct INode node = {.id = 0};
	if(lean_create_inode(&node) == 0) {
		return -ENOSPC;
	}
	
	node.raw.attributes |= typeFlag;
	node.raw.attributes |= mode & ((1 << 12) - 1);
	
	char *dup0 = strdup(path);
	char *dup1 = strdup(path);
	
	struct INode parent;
	lean_get_inode_from_path(dirname(dup0), &parent);
	
	lean_link(&parent, &node, basename(dup1));
	
	free(dup0);
	free(dup1);
	
	lean_save_inode(&node);
	lean_save_inode(&parent);
	
	return 0;
}

int fe_mkdir(const char *path, mode_t mode) {
	return fe_mknod(path, mode | S_IFDIR, 0);
}

int fe_unlink(const char *path) {
	//~ printf("unlink %s\n", path);
	
	char *dup0 = strdup(path);
	char *dup1 = strdup(path);
	
	struct INode child;
	if(lean_get_inode_from_path(path, &child) == 0) return -ENOENT;
	
	struct INode parent;
	if(lean_get_inode_from_path(dirname(dup0), &parent) == 0) return -ENOENT;
	
	lean_unlink(&parent, &child, basename(dup1));
	
	free(dup0);
	free(dup1);
	
	return 0;
}

int fe_write(const char *path, const char *buf, size_t len, off_t off, struct fuse_file_info *fi) {
	//~ printf("write %s %lu %lu\n", path, off, len);
	
	struct INode node;
	if(lean_get_inode(fi->fh, &node) == 0) return -ENOENT;
	
	if(off + len > node.raw.fileSize) {
		node.raw.fileSize = off + len;
		
		if(node.raw.fileSize > (node.raw.blockCount << backend.superblock.logBlockSize)) {
			lean_extend_node(&node, (node.raw.fileSize >> backend.superblock.logBlockSize) - node.raw.blockCount + 1);
		}
	}
	
	lean_write_inode(&node, off, buf, len);
	
	lean_save_inode(&node);
	
	return len;
}

int fe_truncate(const char *path, off_t new_size) {
	//~ printf("truncate %s %lu\n", path, new_size);
	
	struct INode node;
	if(lean_get_inode_from_path(path, &node) == 0) return -ENOENT;
	
	if(node.raw.fileSize >= new_size) {
		lean_shrink_node(&node, (node.raw.fileSize >> backend.superblock.logBlockSize) - (new_size >> backend.superblock.logBlockSize));
	} else {
		if(lean_extend_node(&node, (new_size >> backend.superblock.logBlockSize) - (node.raw.fileSize >> backend.superblock.logBlockSize)) == 0) {
			return -ENOSPC;
		}
	}
	
	node.raw.fileSize = new_size;
	
	lean_save_inode(&node);
	
	return 0;
}

void fe_destroy(void *ud) {
	lean_cleanup();
}

int fe_rmdir(const char *path) {
	return fe_unlink(path);
}

static struct fuse_operations callbacks = {
	.getattr = fe_getattr,
	.open = fe_open,
	.read = fe_read,
	.readdir = fe_readdir,
	.mknod = fe_mknod,
	.unlink = fe_unlink,
	.write = fe_write,
	.truncate = fe_truncate,
	.mkdir = fe_mkdir,
	.destroy = fe_destroy,
	.rmdir = fe_rmdir,
};

static int option_processor(void *data, const char *arg, int key, struct fuse_args *args) {
	if(arg[0] != '-') {
		static int i = 0;
		if(i++ == 1) {
			backend.fd = open(arg, O_RDWR);
			if(lean_load_superblock()) {
				return 0;
			} else return -1;
		} else return 1;
	} else if(strstr(arg, "-C") == arg) {
		uint64_t i;
		
		backend.fd = open(arg + 2, O_RDWR | O_CREAT, 0666);
		
		memset(&backend.superblock, 0, sizeof(backend.superblock));
		backend.superblock.magic = 0x4E41454C;
		backend.superblock.fsVersion = 0x0007;
		backend.superblock.preallocCount = 4;
		backend.superblock.state = 1;
		backend.superblock.badInode = 0;
		backend.superblock.journalInode = 0;
		
		int ur = open("/dev/urandom", O_RDONLY);
		read(ur, backend.superblock.uuid, 16);
		close(ur);
		
		do {
			fputs("Enter block size (must be PoT and >=256) ", stdout);
			if(scanf("%" SCNu64, &i) == 0) continue;
		} while(i < 256 || (i & (i - 1)));
		
		backend.superblock.logBlockSize = __builtin_ctz(i);
		backend.superblock.logBlocksPerBand = __builtin_ctz(i) + 3;
		
		while(1) {
			fputs("Enter size of volume in blocks: ", stdout);
			if(scanf("%" SCNu64, &i) == 1) break;
		}
		
		backend.superblock.blockCount = i;
		backend.superblock.freeBlockCount = i;
		ftruncate(backend.fd, i << backend.superblock.logBlockSize);
		
		do {
			fputs("Enter superblock ID (must be <=32) ", stdout);
			if(scanf("%" SCNu64, &i) == 0) continue;
		} while(i > 32);
		
		backend.superblock.primarySuper = i;
		
		while(1) {
			fputs("Enter backup superblock ID (0 to set to the last block of the first band, which is recommended): ", stdout);
			if(scanf("%" SCNu64, &i) == 1) break;
		}
		
		if(i == 0) {
			i = (1 << backend.superblock.logBlocksPerBand) - 1;
		}
		
		backend.superblock.backupSuper = i;
		
		while(1) {
			fputs("Enter first bitmap portion block (0 means to put directly after superblock, which is recommended): ", stdout);
			if(scanf("%" SCNu64, &i) == 1) break;
		}
		
		if(i == 0) {
			i = backend.superblock.primarySuper + 1;
		}
		
		backend.superblock.bitmapStart = i;
		
		while(1) {
			fputs("Enter ID of root inode (0 means to put directly after the first bitmap portion block, which is recommended): ", stdout);
			if(scanf("%" SCNu64, &i) == 1) break;
		}
		
		if(i == 0) {
			i = backend.superblock.bitmapStart + 1;
		}
		
		backend.superblock.rootInode = i;
		
		backend.superblock.checksum = lean_compute_checksum((uint32_t*) &backend.superblock, (sizeof(struct Superblock)) >> 2, 1 << (backend.superblock.logBlockSize - 2));
		
		for(size_t blk = 0; blk < backend.superblock.primarySuper; blk++) {
			lean_mark_block(blk, 1);
		}
		lean_mark_block(backend.superblock.backupSuper, 1);
		for(size_t band = 0; band < (backend.superblock.blockCount >> backend.superblock.logBlocksPerBand); band++) {
			size_t bitmap;
			if(band == 0) {
				bitmap = backend.superblock.bitmapStart;
			} else {
				bitmap = band << backend.superblock.logBlocksPerBand;
			}
			size_t size = 1 << (backend.superblock.logBlocksPerBand - backend.superblock.logBlockSize - 3);
			for(size_t blk = 0; blk < size; blk++) {
				lean_mark_block(bitmap + blk, 1);
			}
		}
		
		struct INode root = {.id = backend.superblock.rootInode};
		lean_create_inode(&root);
		root.raw.attributes |= INODE_ATTRIBUTE_iaFmtDirectory;
		lean_save_inode(&root);
		
		lean_cleanup();
		
		close(backend.fd);
		
		exit(0);
		
		return 0;
	} else if(strstr(arg, "-O") == arg) {
		backend.offset = strtol(arg + 2, NULL, 0);
		return 0;
	}
	return 1;
}

int main(int argc, char **argv) {
	argv[argc] = "-s"; /* Kind of a crude hack, but it works. */
	
	struct fuse_args args = FUSE_ARGS_INIT(argc + 1, argv);
	fuse_opt_parse(&args, NULL, NULL, option_processor);
	return fuse_main(args.argc, args.argv, &callbacks, NULL);
}