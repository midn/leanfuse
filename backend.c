#include"backend.h"

#include<unistd.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
#include<stdio.h>
#include<signal.h>

struct Backend backend;

static void vread(void *dst, size_t src, size_t len) {
	pread(backend.fd, dst, len, backend.offset + src);
}

static void vwrite(const void *src, size_t dst, size_t len) {
	pwrite(backend.fd, src, len, backend.offset + dst);
}

//~ static int get_physical_location_from_offset(struct INode *node, size_t logical, size_t *block, ssize_t *byte) {
	//~ size_t logicalBlock = logical >> backend.superblock.logBlockSize;
	
	//~ if(node->raw.attributes & INODE_ATTRIBUTE_iaInlineExtAttr) {
		//~ logicalBlock++;
	//~ } else {
		//~ if((logical += 176) >= (1 << backend.superblock.logBlockSize)) {
			//~ logicalBlock++;
		//~ }
	//~ }
	
	//~ for(int e = 0, b = 0; e < node->raw.extentCount; e++, b += node->raw.extentSizes[e]) {
		//~ if(logicalBlock >= b && logicalBlock < b + node->raw.extentSizes[e]) {
			//~ *block = node->raw.extentStarts[e] + logicalBlock - b;
			//~ *byte = logical % (1 << backend.superblock.logBlockSize);
			//~ return 1;
		//~ }
	//~ }
	
	//~ return 0;
//~ }

int lean_get_inode(size_t id, struct INode *out) {
	out->id = id;
	vread(&out->raw, id << backend.superblock.logBlockSize, sizeof(out->raw));
	return 1;
}

void lean_save_inode(const struct INode *in) {
	vwrite(&in->raw, in->id << backend.superblock.logBlockSize, sizeof(in->raw));
}

int lean_extend_node(struct INode *node, size_t blocks) {
	for(size_t b = 0; b < blocks;) {
		if(!lean_is_block_used(node->raw.extentStarts[node->raw.extentCount - 1] + node->raw.extentSizes[node->raw.extentCount - 1])) {
			node->raw.extentSizes[node->raw.extentCount - 1]++;
			b++;
		} else if(node->raw.extentCount == 6) {
			/* TODO: Rewind previous effects. */
			return 0;
		} else {
			int size;
			size_t pb = lean_find_free_blocks(1, blocks - b, &size);
			
			if(pb == -1) {
				/* TODO: Rewind previous effects. */
				return 0;
			}
			
			backend.superblock.freeBlockCount -= pb;
			
			for(size_t i = 0; i < size; i++) {
				lean_mark_block(pb + i, 1);
			}
			
			node->raw.extentStarts[node->raw.extentCount] = pb;
			node->raw.extentSizes[node->raw.extentCount] = size;
			
			node->raw.extentCount++;
			
			b += size;
		}
	}
	node->raw.blockCount += blocks;
	return 1;
}

int lean_shrink_node(struct INode *node, size_t blocks) {
	for(size_t b = 0; b < blocks; b++) {
		lean_mark_block(node->raw.extentStarts[node->raw.extentCount - 1] + --node->raw.extentSizes[node->raw.extentCount - 1], 0);
		if(node->raw.extentSizes[node->raw.extentCount - 1] == 0) {
			node->raw.extentCount--;
		}
		node->raw.blockCount--;
		backend.superblock.freeBlockCount++;
	}
	return 1;
}

void *lean_read_inode(struct INode *inode, size_t off, size_t len) {
	size_t startBlk = (176 + off) >> backend.superblock.logBlockSize;
	size_t endBlk = (176 + off + len) >> backend.superblock.logBlockSize;
	
	if(inode->raw.attributes & INODE_ATTRIBUTE_iaInlineExtAttr) {
		startBlk++, endBlk++;
	}
	
	uint8_t *ret = malloc((endBlk - startBlk + 1) << backend.superblock.logBlockSize);
	memset(ret, 0, (endBlk - startBlk + 1) << backend.superblock.logBlockSize);
	
	/* TODO: Improve. */
	for(size_t blk = startBlk; blk <= endBlk; blk++) {
		for(int e = 0, b = 0; e < inode->raw.extentCount; b += inode->raw.extentSizes[e], e++) {
			if(blk >= b && blk < b + inode->raw.extentSizes[e]) {
				vread(ret + ((blk - startBlk) << backend.superblock.logBlockSize), (inode->raw.extentStarts[e] + blk - b) << backend.superblock.logBlockSize, 1 << backend.superblock.logBlockSize);
				break;
			}
		}
	}
	
	if(startBlk == 0) {
		memmove(ret, ret + 176, len);
	}
	
	return ret;
}

void lean_write_inode(struct INode *inode, size_t off, const char *data, size_t len) {
	if(inode->raw.attributes & INODE_ATTRIBUTE_iaInlineExtAttr) {
		off += 1 << backend.superblock.logBlockSize;
	} else {
		off += 176;
	}
	
	size_t startLB = off >> backend.superblock.logBlockSize;
	size_t endLB = (off + len) >> backend.superblock.logBlockSize;
	
	size_t areaSize = (endLB - startLB + 1) << backend.superblock.logBlockSize;
	char *tmp = malloc(areaSize);
	for(size_t lb = startLB; lb <= endLB; lb++) {
		for(int e = 0, b = 0; e < inode->raw.extentCount; b += inode->raw.extentSizes[e], e++) {
			vread(tmp + ((lb - startLB) << backend.superblock.logBlockSize), (inode->raw.extentStarts[e] + lb - b) << backend.superblock.logBlockSize, 1 << backend.superblock.logBlockSize);
		}
	}
	
	memcpy(tmp + off % (1 << backend.superblock.logBlockSize), data, len);
	
	for(size_t lb = startLB; lb <= endLB; lb++) {
		for(int e = 0, b = 0; e < inode->raw.extentCount; b += inode->raw.extentSizes[e], e++) {
			vwrite(tmp + ((lb - startLB) << backend.superblock.logBlockSize), (inode->raw.extentStarts[e] + lb - b) << backend.superblock.logBlockSize, 1 << backend.superblock.logBlockSize);
		}
	}
}

int lean_append_node_data(struct INode *node, const char *data, size_t len) {
	size_t freeSpace;
	if(node->raw.attributes & INODE_ATTRIBUTE_iaInlineExtAttr) {
		freeSpace = (node->raw.blockCount - 1) << backend.superblock.logBlockSize;
	} else {
		freeSpace = (node->raw.blockCount << backend.superblock.logBlockSize) - 176;
	}
	
	if(freeSpace < len) {
		if(lean_extend_node(node, (len - freeSpace + (1 << backend.superblock.logBlockSize) - 1) >> backend.superblock.logBlockSize)) {
			return lean_append_node_data(node, data, len);
		} else return 0;
	}
	
	lean_write_inode(node, node->raw.fileSize, data, len);
	
	node->raw.fileSize += len;
	
	return 1;
}

int lean_get_inode_from_path(const char *path, struct INode *out) {
	if(path[0] != '/') return 0;
	
	lean_get_inode(backend.superblock.rootInode, out);
	
	if(path[1] == 0) return 1;
	
	while(1) {
		if(*path == 0) return 1;
		else if(out->raw.attributes & INODE_ATTRIBUTE_iaFmtRegular) return 0;
		
		path++;
		
		char name[256] = {};
		for(int i = 0; i < 256; i++) {
			if(*path == '/' || *path == 0) break;
			else name[i] = *(path++);
		}
		
		char *entries = lean_read_inode(out, 0, out->raw.fileSize);
		for(size_t i = 0; i < out->raw.fileSize;) {
			struct DirectoryEntry *e = (void*) &entries[i];
			
			if(e->nameLen == strlen(name) && !memcmp(e->name, name, e->nameLen)) {
				lean_get_inode(e->inode, out);
				goto cont;
			}
			
			i += e->recLen * 16;
		}
		return 0;
cont:
		free(entries);
	}
}

int lean_load_superblock() {
	struct Superblock *pretendant = malloc(256);
	for(size_t i = 512; i <= 135168; i += 512) {
		vread(pretendant, i, 256);
		
		if(pretendant->magic != 0x4E41454C) continue;
		if(i % (1 << pretendant->logBlockSize) != 0) continue;
		
		pretendant = realloc(pretendant, 1 << pretendant->logBlockSize);
		vread((char*) pretendant + 256, i + 256, (1 << pretendant->logBlockSize) - 256);
		
		if(pretendant->checksum != lean_compute_checksum((uint32_t*) pretendant, (1 << pretendant->logBlockSize) >> 2, (1 << pretendant->logBlockSize) >> 2)) continue;
		
		memcpy(&backend.superblock, pretendant, sizeof(struct Superblock));
		
		free(pretendant);
		
		return 1;
	}
	
	return 0;
}

int lean_is_block_used(size_t block) {
	size_t band = block >> backend.superblock.logBlocksPerBand;
	size_t blockInBand = block & ((1 << backend.superblock.logBlocksPerBand) - 1);
	size_t bitmapBlock = (blockInBand >> (3 + backend.superblock.logBlockSize)) + (band << backend.superblock.logBlocksPerBand);
	if(band == 0) {
		bitmapBlock += backend.superblock.bitmapStart;
	}
	int bit = blockInBand & ((1 << backend.superblock.logBlockSize) * 8 - 1);
	
	uint8_t val;
	vread(&val, (bitmapBlock << backend.superblock.logBlockSize) + bit / 8, 1);
	
	return !!(val & (1 << (bit % 8)));
}

void lean_mark_block(size_t block, int used) {
	size_t band = block >> backend.superblock.logBlocksPerBand;
	size_t blockInBand = block & ((1 << backend.superblock.logBlocksPerBand) - 1);
	size_t bitmapBlock = (blockInBand >> (3 + backend.superblock.logBlockSize)) + (band << backend.superblock.logBlocksPerBand);
	if(band == 0) {
		bitmapBlock += backend.superblock.bitmapStart;
	}
	int bit = blockInBand & ((1 << backend.superblock.logBlockSize) * 8 - 1);
	
	uint8_t val;
	vread(&val, (bitmapBlock << backend.superblock.logBlockSize) + bit / 8, 1);
	
	val = (val & (~(1 << (bit % 8)))) | (used << (bit % 8));
	
	vwrite(&val, (bitmapBlock << backend.superblock.logBlockSize) + bit / 8, 1);
}

size_t lean_find_free_blocks(int minSize, int maxSize, int *foundSize) {
	size_t largestFreeSpace = -1, largestFreeSpaceSize = 0;
	
	size_t accum = 0, block = 0;
	for(; block < backend.superblock.blockCount; block++) {
		if(!lean_is_block_used(block)) {
			accum++;
			
			if(accum > largestFreeSpaceSize) {
				largestFreeSpace = block - accum + 1;
				largestFreeSpaceSize = accum;
			}
			
			if(accum == 4) {
				break;
			}
		} else accum = 0;
	}
	
	if(largestFreeSpaceSize < minSize) return -1;
	
	*foundSize = largestFreeSpaceSize;
	return largestFreeSpace;
}

int lean_create_inode(struct INode *node) {
	int size;
	size_t block;
	if(node->id == 0) {
		block = lean_find_free_blocks(1, 4, &size);
		for(int i = 0; i < size; i++) {
			lean_mark_block(block + i, 1);
		}
	} else {
		block = node->id;
		for(size = 0; size < 4; size++) {
			if(lean_is_block_used(block + size)) break;
			else lean_mark_block(block + size, 1);
		}
	}
	
	backend.superblock.freeBlockCount -= size;
	
	memset(node, 0, sizeof(*node));
	node->id = block;
	node->raw.magic = 0x45444F4E;
	node->raw.extentCount = 1;
	node->raw.indirectCount = 0;
	node->raw.linkCount = 0;
	node->raw.uid = 0;
	node->raw.gid = 0;
	node->raw.attributes = 0;
	node->raw.fileSize = 0;
	node->raw.blockCount = size;
	node->raw.accessTime = 0;
	node->raw.statusChangeTime = 0;
	node->raw.modificationTime = 0;
	node->raw.creationTime = 0;
	node->raw.firstIndirect = 0;
	node->raw.lastIndirect = 0;
	node->raw.fork = 0;
	node->raw.extentStarts[0] = block;
	node->raw.extentSizes[0] = size;
	node->raw.checksum = lean_compute_checksum((uint32_t*) &node->raw, (sizeof(node->raw)) >> 2, (sizeof(node->raw)) >> 2);
	
	return 1;
}

void lean_link(struct INode *parent, struct INode *child, const char *name) {
	size_t recLen = (12 + strlen(name) + 15) / 16;
	struct DirectoryEntry *e = alloca(recLen * 16);
	e->inode = child->id;
	e->type = child->raw.attributes >> 29;
	e->recLen = recLen;
	e->nameLen = strlen(name);
	strcpy(e->name, name);
	lean_append_node_data(parent, e, recLen * 16);
	child->raw.linkCount++;
}

static void delete_inode(struct INode *node) {
	if(node->raw.attributes & INODE_ATTRIBUTE_iaFmtDirectory) {
		char *entries = lean_read_inode(node, 0, node->raw.fileSize);
		for(int i = 0; i < node->raw.fileSize;) {
			struct DirectoryEntry *e = (void*) &entries[i];
			
			char *name = malloc(e->nameLen + 1);
			name[e->nameLen] = 0;
			memcpy(name, e->name, e->nameLen);
			
			struct INode child;
			lean_get_inode(e->inode, &child);
			lean_unlink(node, &child, name);
			
			i += e->recLen * 16;
		}
	}
	
	for(int e = 0; e < node->raw.extentCount; e++) {
		for(int b = 0; b < node->raw.extentSizes[e]; b++) {
			lean_mark_block(node->raw.extentStarts[e] + b, 0);
			backend.superblock.freeBlockCount++;
		}
	}
}

void lean_unlink(struct INode *parent, struct INode *child, const char *name) {
	char *entries = lean_read_inode(parent, 0, parent->raw.fileSize);
	for(int i = 0; i < parent->raw.fileSize;) {
		struct DirectoryEntry *e = (void*) &entries[i];
		if(e->type != 0 && e->nameLen == strlen(name) && !strncmp(name, e->name, e->nameLen)) {
			e->inode = 0;
			e->type = 0;
			memset(e->name, 0, e->nameLen);
			e->nameLen = 0;
			lean_write_inode(parent, i, (char*) e, e->recLen * 16);
			
			if(--child->raw.linkCount == 0) {
				delete_inode(child);
			} else {
				lean_save_inode(child);
			}
			return;
		}
		
		i += e->recLen * 16;
	}
}

uint32_t lean_compute_checksum(const uint32_t *buf, size_t dwordLength, size_t dwordPadTo) {
	uint32_t sum = 0;
	
	size_t i = 1;
	for(; i < dwordLength; i++) {
		sum = (sum << 31) + (sum >> 1);
		sum += buf[i];
	}
	
	for(; i < dwordPadTo; i++) {
		sum = (sum << 31) + (sum >> 1);
	}
	
	return sum;
}

void lean_cleanup() {
	backend.superblock.checksum = lean_compute_checksum((uint32_t*) &backend.superblock, (sizeof(struct Superblock)) >> 2, (1 << backend.superblock.logBlockSize) >> 2);
	vwrite(&backend.superblock, backend.superblock.primarySuper << backend.superblock.logBlockSize, sizeof(struct Superblock));
	vwrite(&backend.superblock, backend.superblock.backupSuper << backend.superblock.logBlockSize, sizeof(struct Superblock));
}
