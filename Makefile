all:
	cc -Wall -fno-stack-protector -Os -D_GNU_SOURCE -DFUSE_USE_VERSION=26 -D_FILE_OFFSET_BITS=64 -Wl,-z,norelro -o leanfuse frontend.c backend.c -lfuse
	strip --strip-all -R .comment -R ".note*" -R .eh_frame -R .eh_frame_hdr leanfuse
